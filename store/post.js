import { endpoint } from '~/api/endpoint'

export const state = () => ({
    posts: []
})

export const getters = {
    blogPosts: (state) => state.posts
}

export const actions = {
    async getPosts({commit}) {
        let res = await this.$axios.$get(endpoint.posts.get_posts)
        commit('onOkGetPosts', res)
    },

    async getPost({commit}, id = '') {
        let res = await this.$axios.$get(`${endpoint.posts.get_posts}/${id}`)
        return res
    },

    async addPost({commit}, params = {}) {
        let res = await this.$axios.$post(endpoint.posts.add_post, params)
        commit('onOkAddPost', res)
    },

    async deletePost({commit}, id = '') {
        let res = await this.$axios.$delete(`${endpoint.posts.delete_post}/${id}`)
        console.log('res remove :>> ', res);
        commit('onOkRemovePost', id)
    },

    async updatePost({commit}, params = {}) {
        console.log('params :>> ', params);
        let res = await this.$axios.$patch(`${endpoint.posts.update_post}/${params.id}`,
            params            
        )
        return res
    }
}

export const mutations = {
    onOkGetPosts(state, data) {
        state.posts = data
    },
    onOkAddPost(state, data) {
        state.posts.push(data)
    },
    onOkRemovePost(state, data) {
        let res = state.posts.filter( record => {
            return record.id != data
        })
        state.posts = res
    }
}