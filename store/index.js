export const state = () => ({
    navigation_items: [
        {
            icon: 'mdi-home-variant',
            title: 'Home',
            to: '/'
        },
        {
            icon: 'mdi-book-open',
            title: 'Blogs',
            to: '/blogs'
        }
    ],
    notification_settings: {
        snackbar: false,
        color: 'success',
        y: 'top',
        x: null,
        mode: 'multi-line',
        timeout: -1,
        text: 'Default Message!',
        timer: 3000
    },
})

export const modules = {
    
}

export const getters = {
    navigation_items: (state) => state.navigation_items,
    notification_settings: (state) => state.notification_settings
}

export const actions = {

}

export const mutations = {
    toggleSnackbar(state, data) {
        if (state.notification_settings.snackbar) { state.notification_settings.snackbar = false }
        state.notification_settings.snackbar = !state.notification_settings.snackbar
        state.notification_settings.text = data.text
        state.notification_settings.color = data.color
    },
    forceCloseQuickMessage(state) {
        state.notification_settings.snackbar = false
    }
}