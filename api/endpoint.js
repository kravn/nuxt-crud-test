export const endpoint = {
    posts: {
        get_posts: '/posts',
        add_post: '/posts',
        delete_post: '/posts',
        update_post: '/posts'
    }
}