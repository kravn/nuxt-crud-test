import { endpoint } from './endpoint'
// import axios from 'axios'

export default {
	async getAllPosts(){
		return await axios.get(endpoint.posts.get_posts)
    },
    
    async addPost(params) {
        return await axios.post(endpoint.posts.add_post, params)
    },

    async deletePost(id) {
        return await axios.delete(`${endpoint.posts.add_post}/${id}`)
    },

    async updatePost(params) {
        return await axios.patch(`${endpoint.posts.update_post}/${params.id}`, params)
    }
}